#include <stdio.h>
#include <stdlib.h>

typedef struct players_info *info_node;

typedef struct player *p_pointer;

struct players_info{
	int number;
	p_pointer player1; //node that points to the first node of structure
}players_info;

struct player{
	char name[20];
	char weapon[20];
	p_pointer next;
	p_pointer previous;
}player;

void welcome(void)
{
	printf("Welcome to the game :Rock-Paper-Scissors!\n");
	printf("In this game the computer will predict which player wins.\n");
	printf("Follow the instructions with precission in order to enjoy.\n");
}

info_node set_game()
{
	//creation of players info
	info_node linfo;
	linfo=malloc(sizeof(players_info));
	linfo->number=0;
	linfo->player1=NULL;
	return linfo;
}

void insert_at_start(info_node linfo,p_pointer set_p)
{
	set_p->next=linfo->player1;
	set_p->previous=linfo->player1->next;
	linfo->player1->previous=set_p;
	linfo->player1->next->next=set_p;
	linfo->player1=set_p;
	linfo->number++;
}

void insert_after_first(info_node linfo,p_pointer set_p)
{
	set_p->next=linfo->player1->next;
	set_p->previous=linfo->player1;
	set_p->next->previous=set_p;
	linfo->player1->next=set_p;
	linfo->number++;
}

void set_player(info_node linfo,int *error)
{
	p_pointer set_p;
	set_p=malloc(sizeof(player));
	if(set_p==NULL){ *error=1; return;}
	printf("Give name:");
	fscanf(stdin,"%s",set_p->name);
	printf("Give weapon:");
	fscanf(stdin,"%s",set_p->weapon);
	if(linfo->player1==NULL)
	{
		linfo->player1=set_p;
		set_p->next=linfo->player1;
		set_p->previous=set_p;
		linfo->number++;
	}
	else
	{
		if(((!strcmp(linfo->player1->weapon,"rock"))&&(!strcmp(set_p->weapon,"papper"))) ||
			((!strcmp(linfo->player1->weapon,"papper"))&&(!strcmp(set_p->weapon,"scissors"))) ||
			((!strcmp(linfo->player1->weapon,"scissors"))&&(!strcmp(set_p->weapon,"rock"))))
			{
				insert_at_start(linfo,set_p);
			}
		if(((!strcmp(linfo->player1->weapon,"rock"))&&(!strcmp(set_p->weapon,"scissors"))) ||
			((!strcmp(linfo->player1->weapon,"papper"))&&(!strcmp(set_p->weapon,"rock"))) ||
			((!strcmp(linfo->player1->weapon,"scissors"))&&(!strcmp(set_p->weapon,"papper"))))
			{
				insert_after_first(linfo,set_p);
			}
	}		
}

void who_beats_whom(const info_node linfo,int changes)
{
	char ch='\n';
	p_pointer temp=linfo->player1;
	if(changes%2==0)
	{
		printf("\n\ta)Who wins between %s and %s\n\n",temp->name,temp->next->name);
		temp=temp->next;
		printf("\n\tb)Who wins between %s and %s\n\n",temp->name,temp->next->name);
		temp=temp->next;
		printf("\n\tc)Who wins between %s and %s\n\n",temp->name,temp->next->name);
		printf("\n\tor d)Who wins whom?\n\n");
		printf("\n\tPress 0 to exit!)\n");
		printf("\a\t");
		while(ch=='\n'){ch=getchar();}
		temp=linfo->player1;
		if(ch=='a') {printf("\n\t%s beats %s\n",temp->name,temp->next->name); printf("\t__________________________________________________________|\n"); return;}
		temp=temp->next;
		if(ch=='b') {printf("\n\t%s beats %s\n",temp->name,temp->next->name); printf("\t___________________________________________________________\n"); return;}
		temp=temp->next;
		if(ch=='c') {printf("\n\t%s beats %s\n",temp->name,temp->next->name); printf("\t___________________________________________________________\n"); return;}
		temp=linfo->player1;
		if(ch=='d')
		{
			printf("\n\t%s ",temp->name);
			temp=temp->next;
			do
			{
				printf("beats %s ",temp->name);
				temp=temp->next;
			}while(temp!=linfo->player1);
			return;
		}
		if(ch=='0') return;
	}
	else
	{
		printf("\n\ta)Who wins between %s and %s\n\n",temp->name,temp->previous->name);
		temp=temp->previous;
		printf("\n\tb)Who wins between %s and %s\n\n",temp->name,temp->previous->name);
		temp=temp->previous;
		printf("\n\tc)Who wins between %s and %s\n\n",temp->name,temp->previous->name);
		printf("\n\tor d)Who wins whom?\n\n");
		printf("\n\t(Press 0 to exit!)\n");
		printf("\a\t");
		while(ch=='\n'){ch=getchar();}
		temp=linfo->player1;
		if(ch=='a') {printf("\n\t %s beats %s\n",temp->name,temp->previous->name); printf("\t_____________________________________________________|\n"); return;}
		temp=temp->previous;
		if(ch=='b') {printf("\n\t%s beats %s\n",temp->name,temp->previous->name); printf("\t______________________________________________________|\n"); return;}
		temp=temp->previous;
		if(ch=='c') {printf("\n\t%s beats %s\n",temp->name,temp->previous->name); printf("\t______________________________________________________|\n"); return;}
		temp=linfo->player1;
		if(ch=='d')
		{
			printf("\n\t%s ",temp->name);
			temp=temp->previous;
			do
			{
				printf("beats %s ",temp->name);
				temp=temp->previous;
			}while(temp!=linfo->player1);
			return;
		}
		if(ch=='0') return;
	}
}

void destroy_game(info_node linfo)
{
	p_pointer temp1,temp2;
	temp1=linfo->player1->next;
	while(temp1!=linfo->player1)
	{
		temp2=temp1;
		temp1=temp1->next;
		free(temp2);
	}
	free(linfo->player1);
	linfo->player1=NULL;
	free(linfo);
}

int main(void)
{
	info_node list;
	p_pointer temp=NULL;
	int i,changes,error=0;
	welcome();
	list=set_game();
	printf("Setting game.............................................................\n\n");
	for(i=1;i<=3;i++) set_player(list,&error);
	temp=list->player1;
	printf("%s/%s  ",temp->name,temp->weapon);
	do{
		temp=temp->next;
		printf("beats %s/%s  ",temp->name,temp->weapon);
	}while(temp!=list->player1);
	printf("Checking state of game.....................................................\n\n");
	printf("\tEnter number of changes you've made:(press 0 to exit!)");
	fscanf(stdin,"%d",&changes);
	while(changes!=0)
	{
		getchar();
		who_beats_whom(list,changes);
		printf("\n\tEnter number of changes you've made (press 0 to exit!) :");
		fscanf(stdin,"%d",&i);
		if(i==0) break;
		changes+=i;
		printf("%d(changes)\n",changes);
	}
	printf("Freeing Memmory.....................................\n\n");
	destroy_game(list);
	return 0;	
}
